from sqlalchemy import Column, Integer, String
from database import Base

class Emertime(Base):
    __tablename__ = 'emertime'
    id = Column(Integer, primary_key=True)
    atom_name = Column(String(50), unique=False)
    arch = Column(String(50), unique=False)
    core = Column(Integer)
    memory_mb = Column(Integer)
    time_min = Column(Integer)

    def __init__(
        self, 
        atom_name=None, 
        arch=None,
        core=None,
        memory_mb=None,
        time_min=None
    ):
        self.atom_name = atom_name
        self.arch = arch
        self.core = core
        self.memory_mb = memory_mb
        self.time_min = time_min

    def __repr__(self):
        return "atom name: {atom}\tarch: {arch}\tcores: {core}\ttime (min): {time}\tmemory (mb): {memory}".format(
            time=self.time_min,
            atom=self.atom_name,
            core=self.core,
            arch=self.arch,
            memory=self.memory_mb
            )