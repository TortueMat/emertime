### emertime

this is a simple POC ! please forgive my readme, it should be called `dont readme yet` actually. 
any question / suggestion / feel free to submit an issue


### developed on


```shell
$ python --version
Python 3.7.2
$ uname -a
Linux workstation 4.20.8-arch1-1-ARCH #1 SMP PREEMPT Wed Feb 13 00:06:58 UTC 2019 x86_64 GNU/Linux
$ pip --version
pip 19.0.2 from /home/mathieu/gitlab/tortuemat/emertime/venv/lib/python3.7/site-packages/pip (python 3.7)
$ virtualenv --version
16.0.0
$ docker version
Client:
 Version:           18.09.2-ce
 API version:       1.39
 Go version:        go1.11.5
 Git commit:        62479626f2
 Built:             Mon Feb 11 23:58:17 2019
 OS/Arch:           linux/amd64
 Experimental:      false

Server:
 Engine:
  Version:          18.09.2-ce
  API version:      1.39 (minimum version 1.12)
  Go version:       go1.11.5
  Git commit:       62479626f2
  Built:            Mon Feb 11 23:55:58 2019
  OS/Arch:          linux/amd64
  Experimental:     false
$ docker-compose version
docker-compose version 1.22.0, build f46880fe
docker-py version: 3.4.1
CPython version: 3.6.6
OpenSSL version: OpenSSL 1.1.0f  25 May 2017
```
### start the app (bare metal)

be careful, you'll need to install a postgresql database

```shell
$ pwd
/home/mathieu/gitlab/tortuemat/emertime
$ virtualenv venv
$ source venv/bin/activate
(venv) $ pip install --requirement requirements.txt
(venv) $ FLASK_APP=main.py flask run
```

### start the app with compose:

```
$ docker build -t emertime .
$ docker-compose up -d database emertime
```

profit
