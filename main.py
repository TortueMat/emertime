from flask import Flask, render_template, request

from database import init_db, db_session
from models import Emertime

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/result", methods = ["POST"])
def result():
        if request.method == "POST":
                result = request.form
                atom_name = result["atom"]
                
                return render_template(
                        "result.html", 
                        atom_name=atom_name, 
                        emertimes=get_emertimes(atom_name)
                )

def get_emertimes(atom=None):
        """
        Return emertime for the given atom
        """
        return db_session.query(Emertime).filter(Emertime.atom_name==atom).all()

if __name__ == "__main__":
        init_db()
        app.run(host="0.0.0.0")
